let taskList = [];
let doneTask = [];
// Hàm render
renderList = (item) =>{
    let renderItem = item.map(data =>{
        return `<li class='listElement'>
                    <p>${data}</p>
                    <div>
                        <i onclick='deleteTask("${data}")' style='padding-top:10px' class="fa fa-trash-alt"></i>
                        <i onclick='checkDoneTask("${data}")' style='padding-top:10px;' class="fa fa-check-circle"></i>
                    </div>
                </li class='listElement'> `
    });
    document.querySelector('#todo').innerHTML= `${renderItem.join('')}`
}
// Hàm render nhiệm vụ đã hoàn thành
renderDoneTask = (param) =>{
    let renderItem = param.map(data =>{
        return `<li class='listElement'>
                    <p style='color:green'>${data}</p>
                    <div>
                        <i onclick='editDoneTask("${data}")' style='padding-top:10px' class="fa fa-trash-alt"></i>
                        <i style='padding-top:10px; color:green' class="fa fa-check-circle"></i>
                    </div>
                </li class='listElement'> `
    });
    document.querySelector('#completed').innerHTML = `${renderItem.join('')}`
}

// Thêm vào mảng taskList khi click nút add
document.querySelector('#addItem').addEventListener("click",function(){
    let newTask = document.querySelector('#newTask').value;
    if (!newTask){
        alert("Please enter an activity");
        return;
    }
    taskList.push(newTask);;
    renderList(taskList)
});

// hàm xóa  mục bên trên
deleteTask = (item) =>{
    findIndex = taskList.findIndex(data => data === item);
    taskList.splice(findIndex,1);
    renderList(taskList);
}
// Hàm Xóa mục bên dưới và  hoàn tác
editDoneTask = (item) =>{
    findIndex = doneTask.findIndex(data => data === item);
    doneTask.splice(findIndex,1);
    renderDoneTask(doneTask);
    taskList.push(item);
    renderList(taskList);
}
// Hàm show việc đã làm
checkDoneTask = (item)=>{
    let findDoneTask = taskList.find(pr => {
        return pr == item;
    });
    doneTask.push(findDoneTask)
    renderDoneTask(doneTask);

    let reloadTaskList = taskList.findIndex(data => {
        return data === item
    });
    taskList.splice(reloadTaskList,1);
    renderList(taskList);
}
// Xếp A-Z
document.querySelector('#two').addEventListener("click",function(){
    taskList.sort();
    renderList(taskList);
})
// Xếp Z-A
document.querySelector('#three').addEventListener("click",function(){
    taskList.sort();
    taskList.reverse();
    renderList(taskList);
})
